package com.sks.demo.custom_list;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.magic.customview.widget.BadgeView;
import com.sks.demo.custom_list.list.CustomListAdapter;

public class SectionRowItemAdapter extends CustomListAdapter implements OnClickListener {

	protected List<Pair<String, List<Object>>> all;
	protected LayoutInflater inflater;
	protected Context cont;
	protected IHeaderClick headerListener;

	public SectionRowItemAdapter(Context c, Data data, IHeaderClick inte) {
		cont = c;
		headerListener = inte;
		inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		try{
			all = data.getAllData();
		}catch (Exception e)
		{
			all= new ArrayList<Pair<String,List<Object>>>();
			e.printStackTrace();
		}
	}

	@Override
	public int getCount() {
		int res = 0;
		int len = all.size();
		for (int i = 0; i < len; i++) {
			res += all.get(i).second.size();
			// Log.e("test","taille;"+len+" i:"+i);
		}
		// res = all.get(0).second.size();
		// Log.e("test","taille;"+len);
		return res;
	}

	@Override
	public Object getItem(int position) {
		int c = 0;
		int len=all.size();
		for (int i = 0; i < len; i++) {
			if (position >= c && position < c + all.get(i).second.size()) {
				return all.get(i).second.get(position - c);
			}
			c += all.get(i).second.size();
		}
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	protected void bindSectionHeader(View view, int position, boolean displaySectionHeader) {
		if (displaySectionHeader) {
			view.findViewById(getRessourceHeader()).setVisibility(View.VISIBLE);
			TextView lSectionTitle = (TextView) view.findViewById(getRessourceHeader());
			lSectionTitle.setText(getSections()[getSectionForPosition(position)]);
			lSectionTitle.setOnClickListener(this);
		} else {
			TextView lSectionTitle = (TextView) view.findViewById(getRessourceHeader());
			lSectionTitle.setText(getSections()[getSectionForPosition(position)]);
			lSectionTitle.setOnClickListener(this);
			view.findViewById(getRessourceHeader()).setVisibility(View.GONE);
		}
	}

	@Override
	public View getAmazingView(int position, View convertView, ViewGroup parent) {
		View res = convertView;
		if (res == null)
			res = inflater.inflate(getRessourceLayout(), null);

		TextView ltxt = (TextView) res.findViewById(getRessourceText());
		BadgeView lbadge = (BadgeView) res.findViewById(getRessourceBadge());

		RowItem rowItem = (RowItem) getItem(position);
		ltxt.setText(rowItem.text);
		lbadge.setText(rowItem.badge);
		return res;
	}

	@Override
	public void configurePinnedHeader(View header, int position, int alpha) {
		TextView lSectionHeader = (TextView) header;
		lSectionHeader.setText(getSections()[getSectionForPosition(position)]);
	}

	@Override
	public int getPositionForSection(int section) {
		int len = all.size();
		if (section < 0)
			section = 0;
		if (section >= len)
			section = len - 1;
		int c = 0;
		for (int i = 0; i < len; i++) {
			if (section == i) {
				return c;
			}
			c += all.get(i).second.size();
		}
		return 0;
	}

	@Override
	public int getSectionForPosition(int position) {
		int c = 0;
		int len = all.size();
		for (int i = 0; i < len; i++) {
			if (position >= c && position < c + all.get(i).second.size()) {
				return i;
			}
			c += all.get(i).second.size();
		}
		return 0;
	}

	public boolean isFirstPositionInSection(int position){
		return (getPositionForSection(getSectionForPosition(position)) == position);
	}
	
	public String getSectionNameForPosition(int position) {
		int c = 0;
		int len = all.size();
		for (int i = 0; i < len; i++) {
			if (position >= c && position < c + all.get(i).second.size()) {
				return all.get(i).first;
			}
			c += all.get(i).second.size();
		}
		return null;
	}
	
	@Override
	public String[] getSections() {
		String[] res = new String[all.size()];
		for (int i = 0; i < all.size(); i++) {
			res[i] = all.get(i).first;
		}
		return res;
	}

	protected int getRessourceLayout() {
		return R.layout.row_item;
	}

	protected int getRessourceHeader() {
		return R.id.list_section_header;
	}

	protected int getRessourceText() {
		return R.id.list_section_title1;
	}

	protected int getRessourceBadge() {
		return R.id.list_section_title2;
	}

	@Override
	public void onClick(View arg0) {
		if (headerListener != null)
			headerListener.onListViewHeaderClick(arg0);
	}
}