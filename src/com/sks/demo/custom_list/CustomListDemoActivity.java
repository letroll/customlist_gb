package com.sks.demo.custom_list;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

import com.magic.customview.widget.BadgeView;
import com.sks.demo.custom_list.list.CustomListView;

public class CustomListDemoActivity extends Activity {

	final String tag = "customlist";
	int cpt = 0;
	CustomListView lsRowItem;
	SectionRowItemAdapter adapter;
	Data data;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		data = new Data();
		lsRowItem = (CustomListView) findViewById(R.id.list_services);
		setupList();
	}

	private void setupList() {
		data.addTitle("toto");
		data.addTitle("titi");
		data.addTitle("toto");
		data.addTitle("titi");
		data.addTitle("toto");
		data.addTitle("titi");
		ArrayList<Object> tata = new ArrayList<Object>();
		tata.add(new RowItem("text", "badge"));
		tata.add(new RowItem("text", "badge"));
		tata.add(new RowItem("text", "badge"));

		ArrayList<Object> tete = new ArrayList<Object>();
		tete.add(new RowItem("ttututu", "qdfsdf"));
		tete.add(new RowItem("tutu", "q"));
		tete.add(new RowItem("ttutu", "qd"));
		tete.add(new RowItem("ttututu", "qdfsdf"));

		data.addRowItem(tata);
		data.addRowItem(tete);
		data.addRowItem(tata);
		data.addRowItem(tete);
		data.addRowItem(tata);
		data.addRowItem(tete);
		data.addRowItem(tata);
		data.addRowItem(tete);

		/** ici pour les entetes sticky **/
		// lsRowItem.setPinnedHeaderView(LayoutInflater.from(this).inflate(R.layout.row_header,
		// lsRowItem, false));
		lsRowItem.setAdapter(adapter = new SectionRowItemAdapter(this, data,null));
		lsRowItem.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				String text, badge, header;

				header = ((TextView) view.findViewById(R.id.list_section_header)).getText().toString();
				text = ((TextView) view.findViewById(R.id.list_section_title1)).getText().toString();
				badge = ((BadgeView) view.findViewById(R.id.list_section_title2)).getText().toString();

				Log.v(tag, "Name : " + text);
				Log.v(tag, "Address : " + badge);
				Log.v(tag, "Title: " + header);

			}
		});

	}
}