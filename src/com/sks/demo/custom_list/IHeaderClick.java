package com.sks.demo.custom_list;

import android.view.View;

public interface IHeaderClick {
	public abstract void onListViewHeaderClick(View v);
}
