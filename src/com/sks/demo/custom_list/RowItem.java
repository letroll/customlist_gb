package com.sks.demo.custom_list;

public class RowItem {	
	public String text;
	public String badge;
	
	public RowItem(String text, String badge) {
		this.text = text;
		this.badge = badge;
	}
}
