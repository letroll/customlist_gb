package com.sks.demo.custom_list;

import java.util.ArrayList;
import java.util.List;

import android.util.Pair;

public class Data {

	private ArrayList<String> titles = new ArrayList<String>();
	private ArrayList<ArrayList<Object>> RowItems = new ArrayList<ArrayList<Object>>();

	public void clear() {
		titles.clear();
		RowItems.clear();
	}
	
	public void set(List<Pair<String, List<Object>>> lis){
		clear();
		for (Pair<String, List<Object>> iterable_element : lis) {
	        titles.add(iterable_element.first);
	        RowItems.add((ArrayList<Object>) iterable_element.second);
        }
	}

	public void addTitle(String title) {
		titles.add(title);
	}

	public void addRowItem(ArrayList<Object> array) {
		RowItems.add(array);
	}

	public ArrayList<Object> getArrayForTitle(String title) {
		int len = titles.size();
		for (int i = 0; i < len; i++) {
			if (titles.get(i).equals(title))
				return RowItems.get(i);
		}
		return null;
	}

	public void addRowUniqueItemFirst(Object object, int index) {
		addRowUniqueItem(object, index, 0);
	}

	public void addRowUniqueItem(Object object, int index) {
		addRowUniqueItem(object, index, getAllData().get(index).second.size());
	}

	public void addRowUniqueItem(Object object, int index, int indexlist) {
		List<Pair<String, List<Object>>> dat = getAllData();
		dat.get(index).second.add(indexlist, object);
	}

	public List<Pair<String, List<Object>>> getAllData() {
		List<Pair<String, List<Object>>> res = new ArrayList<Pair<String, List<Object>>>();
		for (int i = 0; i < titles.size(); i++)
			res.add(getOneSection(i));
		return res;
	}

	public List<Object> getFlattenedData() {
		List<Object> res = new ArrayList<Object>();
		for (int i = 0; i < titles.size(); i++)
			res.addAll(getOneSection(i).second);
		return res;
	}

	public Pair<String, List<Object>> getOneSection(int index) {
		return new Pair<String, List<Object>>(titles.get(index), RowItems.get(index));
	}
}
